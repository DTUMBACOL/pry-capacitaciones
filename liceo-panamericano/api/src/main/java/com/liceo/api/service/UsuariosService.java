/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.liceo.api.service;

import com.liceo.api.entities.Usuarios;
import com.liceo.api.model.UsuarioModel;

import java.util.List;

/**
 *
 * @author USUARIO
 */
public interface UsuariosService extends BaseService<Usuarios, Integer> {
        Usuarios findByUsername(String username);

        List<UsuarioModel> findByCapacitacion(Integer capacitacion);
         List<UsuarioModel> findByAsistenciaCapacitacion(Integer capacitacion);
}
