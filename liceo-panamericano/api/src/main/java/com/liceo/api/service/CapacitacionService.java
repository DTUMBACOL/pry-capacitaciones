/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.liceo.api.service;

import com.liceo.api.entities.Capacitacion;
import com.liceo.api.entities.Usuarios;

import java.util.List;

/**
 *
 * @author USUARIO
 */
public interface CapacitacionService extends BaseService<Capacitacion, Integer> {
    List<Capacitacion> findByCapacitacion(Integer usuario)throws Exception;
    List<Capacitacion> findByTalleres(Integer usuario)throws Exception;
    List<Capacitacion> findByUsuario(Integer usuario)throws Exception;

}
