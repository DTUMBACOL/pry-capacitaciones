package com.liceo.api.repository;

import com.liceo.api.entities.Asistencia;
import com.liceo.api.entities.Capacitacion;
import com.liceo.api.entities.Usuarios;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface AsistenciaRepository extends BaseRepository<Asistencia, Integer>  {

    @Query(value = "select * from asistencia where id_capacitacion = :capacitacion ",
            nativeQuery = true)
    public List<Asistencia> findByIdCapacitacion(@Param("capacitacion") int capacitacion);
}
