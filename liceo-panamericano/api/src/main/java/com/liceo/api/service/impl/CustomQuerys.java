/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.liceo.api.service.impl;

import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;
import javax.persistence.EntityManager;
import javax.persistence.Query;

import com.liceo.api.entities.Capacitacion;
import com.liceo.api.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.NativeQuery;
import org.hibernate.transform.AliasToEntityMapResultTransformer;
import org.hibernate.transform.Transformers;

/**
 *
 * @author USUARIO
 */
@Service
@Transactional //It is not necessary. You can use it, if you have multiple database operation in a single service method.
public class CustomQuerys {

    @Autowired
    EntityManager em;

    public CustomQuerys() {
    }

    public boolean validaExisteCedula(String cedula) {

        try {
            String q = "select count(*) from RPEMPLEA WHERE CEDULA = :cedula ";

            NativeQuery query = em.createNativeQuery(q).unwrap(org.hibernate.query.NativeQuery.class);
            query.setParameter("cedula", cedula);

            int count = ((Number) query.getSingleResult()).intValue();

            return count > 0;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;

    }
    public ArrayList<UsuarioModel> getAsistencia(Integer capacitacion) {
        ArrayList<UsuarioModel> list = new ArrayList<>();
        try {
            String q = "select u.id as id , u.nombre as nombre , a.estado as estado , a.id as id_asistencia  from usuarios u inner join ASISTENCIA a ON u.id = a.id_usuario where a.id_capacitacion =:capacitacion ";

            NativeQuery query = em.createNativeQuery(q).unwrap(org.hibernate.query.NativeQuery.class);
            query.setParameter("capacitacion", capacitacion);
            List<Object[]> data = query.getResultList();
            for (Object[] x : data) {
                list.add(new UsuarioModel(x));
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;

    }
    public String getNombreCedula(String cedula) {

        try {
            String q = "select CONCAT(NOMBRES, ' ', APELLIDOS) from RPEMPLEA WHERE CEDULA =  :cedula ";

            NativeQuery query = em.createNativeQuery(q).unwrap(org.hibernate.query.NativeQuery.class);
            query.setParameter("cedula", cedula);

            String nombre = ((String) query.getSingleResult());

            return nombre;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;

    }

    private static final String ALPHA_CAPS = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890";

    public String getRandomSpecialChars(int count) {
        SecureRandom random = new SecureRandom();
        String result = "";
        for (int i = 0; i < count; i++) {
            int index = random.nextInt(ALPHA_CAPS.length());
            result += ALPHA_CAPS.charAt(index);
        }
        return result;
    }


    public ReporteDTO findReporte(String fecha){
        ReporteDTO dto = new ReporteDTO();
        dto.setTalleres(findTalleresCapacitaciones("T",fecha));
        dto.setCapacitaciones(findTalleresCapacitaciones("C",fecha));
        HorasReporte d = new HorasReporte();
        int totalCapacitacion = 0;
        totalCapacitacion =  dto.getCapacitaciones().stream().map(t -> t.getDuracion()).reduce(totalCapacitacion, (accumulator, _item) -> accumulator + _item);
        int totalTalleres = 0;
        totalTalleres =  dto.getTalleres().stream().map(t -> t.getDuracion()).reduce(totalTalleres, (accumulator, _item) -> accumulator + _item);
        d.setHoraTalleres(totalTalleres);
        d.setHoraCapacitacion(totalCapacitacion);
        dto.setHoras(d);

        dto.setDocentes(findUsuarios(fecha));


        return dto;
    }

    public  List<CapacitacionReporte> findTalleresCapacitaciones(String tipo, String fecha) {
        List<CapacitacionReporte> list = null;

        try {
            String q = "select c.* from capacitacion c where c.tipo = :tipo and c.fecha = :fecha";

            Query query = em.createNativeQuery(q , Capacitacion.class);
            query.setParameter("tipo", tipo);
            query.setParameter("fecha", fecha);
            list = ((List<Capacitacion>) query.getResultList()).stream().map(t -> CapacitacionReporte.builder()
                    .hora(t.getHorario())
                    .fecha(t.getFecha())
                    .nombre(t.getTitulo())
                    .duracion(t.getDuracion()).build()).collect(Collectors.toList());

            return list;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public  List<UsuarioReporte> findUsuarios(String fecha) {
        List<UsuarioReporte> list = new ArrayList<>();

        try {
            String q = "select c.titulo, u.nombre , u.cedula from ASISTENCIA a inner JOIN CAPACITACION c on a.id_capacitacion = c.id inner join  USUARIOS u on a.id_usuario = u.id where a.estado = 'A' and c.fecha = :fecha";

            Query query = em.createNativeQuery(q );
            query.setParameter("fecha", fecha);

            List<Object[]> data =   query.getResultList();
                    data.forEach(o ->
            {

                list.add(UsuarioReporte.builder()
                        .capacitacion(String.valueOf(o[0]))
                        .nombre(String.valueOf(o[1]))
                        .cedula(String.valueOf(o[2]))
                        .build());

            });

            return list;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }



}
