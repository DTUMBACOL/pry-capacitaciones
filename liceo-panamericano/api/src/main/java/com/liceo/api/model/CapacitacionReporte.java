package com.liceo.api.model;

import lombok.*;

import javax.persistence.Entity;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CapacitacionReporte {

    String nombre;
    String hora;
    String fecha;
    int duracion;

}
