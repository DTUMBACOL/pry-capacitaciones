package com.liceo.api.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ReporteDTO {


    List<CapacitacionReporte> talleres;
    List<CapacitacionReporte> capacitaciones;
    List<UsuarioReporte> docentes;
    HorasReporte horas;
}
