/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.liceo.api.repository;

import com.liceo.api.entities.Base;
import com.liceo.api.entities.Capacitacion;
import com.liceo.api.entities.Usuarios;
import java.io.Serializable;
import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.data.repository.query.Param;

/**
 *
 * @author USUARIO
 */
public interface CapacitacionRepository extends BaseRepository<Capacitacion, Integer> {

    @Query(value = "select * from CAPACITACION where tipo = :tipo and id not in ( select id_capacitacion from ASISTENCIA where id_usuario = :usuario )",
            nativeQuery = true)
    public List<Capacitacion> findByTipo(@Param("tipo")String tipo,@Param("usuario") Integer usuario);


    @Query(value = "select * from CAPACITACION where id in ( select id_capacitacion from ASISTENCIA where id_usuario = :usuario )",
            nativeQuery = true)
    public List<Capacitacion> findByAsistencia(@Param("usuario") Integer usuario);

}
