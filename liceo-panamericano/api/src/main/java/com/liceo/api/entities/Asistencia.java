package com.liceo.api.entities;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Date;


@Entity
@Table(name = "ASISTENCIA")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Asistencia  extends Base{
    int id_capacitacion;
    int id_usuario;
    Date fecha_asistencia;
    String estado;

}
