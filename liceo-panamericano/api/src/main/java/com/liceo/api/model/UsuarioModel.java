package com.liceo.api.model;

import com.liceo.api.entities.Usuarios;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class UsuarioModel {
    int id;
    String cedula;
    String rol;
    String email;
    String nombre;
    String estado;
    int id_asistencia;
    boolean select;

    public UsuarioModel(Usuarios u){
        this.id = u.getId();
        this.cedula  = u.getCedula();
        this.rol = u.getRol();
        this.email = u.getEmail();
        this.nombre = u.getNombre();
//        this.estado = u.getNombre();
//        this.select = u.getNombre();

    }


    public UsuarioModel(Object[] x) {
        this.id = Integer.valueOf(String.valueOf(x[0]));
        this.nombre =  String.valueOf(x[1]);
        this.estado = String.valueOf(x[2]);
        this.select = this.estado.equals("A")? true:false;
        this.id_asistencia = Integer.valueOf(String.valueOf(x[3]));
    }
}
