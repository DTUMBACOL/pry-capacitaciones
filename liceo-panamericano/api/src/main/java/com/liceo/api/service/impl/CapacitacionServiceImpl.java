/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.liceo.api.service.impl;

import com.google.common.hash.Hashing;
import com.liceo.api.entities.Capacitacion;
import com.liceo.api.entities.Usuarios;
import com.liceo.api.repository.BaseRepository;
import com.liceo.api.repository.CapacitacionRepository;
import com.liceo.api.repository.UsuariosRepository;
import com.liceo.api.service.CapacitacionService;
import com.liceo.api.service.UsuariosService;
import com.liceo.api.util.SendMail;
import java.nio.charset.StandardCharsets;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author USUARIO
 */
@Service
@Transactional //It is not necessary. You can use it, if you have multiple database operation in a single service method.
public class CapacitacionServiceImpl extends BaseServiceImpl<Capacitacion, Integer> implements CapacitacionService {

    @Autowired
    CustomQuerys querys;

    @Autowired
        private CapacitacionRepository repository;

    public CapacitacionServiceImpl(BaseRepository<Capacitacion, Integer> baseRepository) {
        super(baseRepository);
    }


    @Override
    public List<Capacitacion> findByCapacitacion(Integer usuario) throws Exception{
        try {
            List<Capacitacion> entities = repository.findByTipo("C",usuario);
            return entities;
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        }
    }

    @Override
    public List<Capacitacion> findByTalleres(Integer usuario) throws Exception{
        try {
            List<Capacitacion> entities = repository.findByTipo("T", usuario);
            return entities;
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        }
    }

    @Override
    public List<Capacitacion> findByUsuario(Integer usuario) throws Exception {
        try {
            List<Capacitacion> entities = repository.findByAsistencia(usuario);
            return entities;
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        }    }
}
