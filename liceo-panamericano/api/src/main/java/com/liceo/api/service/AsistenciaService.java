/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.liceo.api.service;

import com.liceo.api.entities.Asistencia;
import com.liceo.api.entities.Capacitacion;
import com.liceo.api.model.AsistenciaDTO;

import java.util.List;

/**
 *
 * @author USUARIO
 */
public interface AsistenciaService extends BaseService<Asistencia, Integer> {

    List<Asistencia> saveAll(AsistenciaDTO dto) throws Exception;

}
