/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.liceo.api.service.impl;

import com.google.common.hash.Hashing;
import com.liceo.api.entities.Usuarios;
import com.liceo.api.model.UsuarioModel;
import com.liceo.api.repository.BaseRepository;
import com.liceo.api.repository.UsuariosRepository;
import com.liceo.api.service.UsuariosService;
import com.liceo.api.util.SendMail;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author USUARIO
 */
@Service
@Transactional //It is not necessary. You can use it, if you have multiple database operation in a single service method.
public class UsuariosServiceImpl extends BaseServiceImpl<Usuarios, Integer> implements UsuariosService {

    @Autowired
    CustomQuerys querys;

    @Autowired
    private UsuariosRepository repository;

    public UsuariosServiceImpl(BaseRepository<Usuarios, Integer> baseRepository) {
        super(baseRepository);
    }

    @Override
    public Usuarios findByUsername(String username) {

        return repository.findByCedula(username).orElse(null);
    }

    @Override
    public List<UsuarioModel> findByCapacitacion(Integer capacitacion) {
        return  repository.findByCapacitacion(capacitacion).stream().map(t -> new UsuarioModel(t)).collect(Collectors.toList());
    }

    @Override
    public List<UsuarioModel> findByAsistenciaCapacitacion(Integer capacitacion) {
        return  querys.getAsistencia(capacitacion);
    }

    @Override
    public Usuarios save(Usuarios entity) throws Exception {
        try {
            if (querys.validaExisteCedula(entity.getCedula()) && findByUsername(entity.getCedula()) == null) {
                String password = querys.getRandomSpecialChars(8);
                SendMail mail = new SendMail();
                mail.sendmail(entity.getEmail(), password, "Clave");
                entity.setEstado("A");
                entity.setRol("DOCENTE");
                entity.setNombre(querys.getNombreCedula(entity.getCedula()));
                BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();

                entity.setClave(passwordEncoder.encode(password));


                return super.save(entity);

            } else {
                throw new Exception("El numero de cedula no es valido");
            }

        } catch (Exception e) {

            throw new Exception(e.getMessage());

        }
    }

    public Usuarios reset(Usuarios entity) throws Exception {
        try {
            if (querys.validaExisteCedula(entity.getCedula()) && findByUsername(entity.getCedula()) != null) {
                entity = findByUsername(entity.getCedula());

                String password = querys.getRandomSpecialChars(8);
                SendMail mail = new SendMail();
                mail.sendmail(entity.getEmail(), password, "Clave");
                BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();

                entity.setClave(passwordEncoder.encode(password));

                return super.save(entity);

            } else {
                throw new Exception("El numero de cedula no es valido");
            }

        } catch (Exception e) {
            throw new Exception(e.getMessage());

        }
    }

    public Usuarios valida(Usuarios entity) throws Exception {
        try {
            if (querys.validaExisteCedula(entity.getCedula()) && findByUsername(entity.getCedula()) != null) {
                Usuarios entityBd = findByUsername(entity.getCedula());

                String sha256hex = Hashing.sha256()
                        .hashString(entity.getClave(), StandardCharsets.UTF_8)
                        .toString();
                if (sha256hex.equals(entityBd.getClave()) && entityBd.getEstado().equals("A")) {
                    entity.setRol(entityBd.getRol());
                    entity.setEmail(entityBd.getEmail());
                    entity.setId(entityBd.getId());
                    return entity;
                } else {
                    throw new Exception("Datos no validos");
                }
            } else {
                throw new Exception("El numero de cedula no es valido");
            }

        } catch (Exception e) {
            throw new Exception(e.getMessage());

        }
    }

}
