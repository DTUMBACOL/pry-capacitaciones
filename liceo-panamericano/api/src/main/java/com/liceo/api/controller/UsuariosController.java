/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.liceo.api.controller;

import com.google.common.hash.Hashing;
import com.liceo.api.entities.Usuarios;
import com.liceo.api.service.impl.CustomQuerys;
import com.liceo.api.service.impl.UsuariosServiceImpl;
import com.liceo.api.util.SendMail;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Comparator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.mail.MessagingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 *
 * @author USUARIO
 */
@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/api/v1/usuarios")
public class UsuariosController extends BaseControllerImpl<Usuarios, UsuariosServiceImpl> {

    private UsuariosServiceImpl cooperativaServiceImpl;

    @Autowired
    UsuariosServiceImpl service;

    public UsuariosController(UsuariosServiceImpl cooperativaServiceImpl) {
        this.cooperativaServiceImpl = cooperativaServiceImpl;
    }

    @PostMapping("/reset")
    public ResponseEntity<?> reset(@RequestBody Usuarios entity) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(servicio.reset(entity));
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
        }
    }

    @PostMapping("/valida")
    public ResponseEntity<?> valida(@RequestBody Usuarios entity) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(servicio.valida(entity));
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
        }
    }

    @GetMapping("/capacitacion")
    public ResponseEntity<?> valida(@RequestParam(name = "capacitacion") int capacitacion) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(servicio.findByAsistenciaCapacitacion(capacitacion));
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
        }
    }
}
