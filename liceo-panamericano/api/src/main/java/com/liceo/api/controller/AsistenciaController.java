/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.liceo.api.controller;

import com.liceo.api.entities.Asistencia;
import com.liceo.api.entities.Usuarios;
import com.liceo.api.model.AsistenciaDTO;
import com.liceo.api.service.impl.AsistenciaServiceImpl;
import com.liceo.api.service.impl.UsuariosServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 *
 * @author USUARIO
 */
@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/api/v1/asistencia")
public class AsistenciaController extends BaseControllerImpl<Asistencia, AsistenciaServiceImpl> {

    private AsistenciaServiceImpl asistenciaService;

    public AsistenciaController(AsistenciaServiceImpl asistenciaService) {
        this.asistenciaService = asistenciaService;
    }

    @PostMapping("/registra")
    public ResponseEntity<?> valida(@RequestBody AsistenciaDTO asistencias  ) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(servicio.saveAll(asistencias));
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
        }
    }

}
