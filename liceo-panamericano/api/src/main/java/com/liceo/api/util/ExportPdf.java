package com.liceo.api.util;

import com.liceo.api.model.CapacitacionReporte;
import com.liceo.api.model.ReporteDTO;
import com.liceo.api.model.UsuarioReporte;
import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Font;
import com.lowagie.text.FontFactory;
import com.lowagie.text.PageSize;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Phrase;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;
import java.awt.Color;
import java.io.IOException;
import java.util.List;
import javax.servlet.http.HttpServletResponse;


public class ExportPdf {

    ReporteDTO reporteDTO;

    public ExportPdf( ReporteDTO reporteDTO ) {
        this.reporteDTO = reporteDTO;
    }




    private void writeTableHeaderDocente(PdfPTable table) {
        PdfPCell cell = new PdfPCell();
        cell.setBackgroundColor(Color.BLACK);
        cell.setPadding(0);

        Font font = FontFactory.getFont(FontFactory.HELVETICA);
        font.setColor(Color.WHITE);
        cell.setPhrase(new Phrase("Docente", font));
        table.addCell(cell);
        cell.setPhrase(new Phrase("Actividad", font));
        table.addCell(cell);
    }

    private void writeTableDataDocente(PdfPTable table) {
        for (UsuarioReporte item : reporteDTO.getDocentes()) {
            table.addCell(String.valueOf(item.getNombre()));
            table.addCell(item.getCapacitacion());
        }
    }

    private void writeTableHeaderActividad(PdfPTable table) {
        PdfPCell cell = new PdfPCell();
        cell.setBackgroundColor(Color.BLACK);
        cell.setPadding(0);

        Font font = FontFactory.getFont(FontFactory.HELVETICA);
        font.setColor(Color.WHITE);
        cell.setPhrase(new Phrase("Actividad", font));
        table.addCell(cell);
        cell.setPhrase(new Phrase("Hora", font));
        table.addCell(cell);
        cell.setPhrase(new Phrase("Duracion", font));
        table.addCell(cell);
    }


    private void writeTableDataActividad(PdfPTable table, List<CapacitacionReporte> list) {
        for (CapacitacionReporte item :list) {
            table.addCell(item.getNombre());
            table.addCell(item.getHora());
            table.addCell(String.valueOf(item.getDuracion()));
        }
    }

    public void export(HttpServletResponse response) throws DocumentException, IOException {
        Document document = new Document(PageSize.A4);
        PdfWriter.getInstance(document, response.getOutputStream());

        document.open();

        Font font = FontFactory.getFont(FontFactory.HELVETICA_BOLD);
        font.setSize(11);
        font.setColor(Color.BLACK);

        Paragraph p = new Paragraph("Reportes de Sistema de Capacitaciones y Talleres", font);
        p.setAlignment(Paragraph.ALIGN_CENTER);

        document.add(p);

        PdfPTable table = new PdfPTable(2);
        table.setWidthPercentage(100f);
//        table.setWidthPercentage(7.5f);
        table.setSpacingBefore(10);

        writeTableHeaderDocente(table);
        writeTableDataDocente(table);

        table.setSpacingBefore(10);
        PdfPTable table2 = new PdfPTable(3);


        table2.setWidthPercentage(100f);
        table2.setSpacingBefore(10);
        writeTableHeaderActividad(table2);

        writeTableDataActividad(table2,reporteDTO.getCapacitaciones());
        writeTableDataActividad(table2,reporteDTO.getTalleres());



        table2.setSpacingBefore(10);


        document.add(table);
        document.add(table2);

        document.close();

    }
}
