/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.liceo.api.service.impl;

import com.liceo.api.entities.Asistencia;
import com.liceo.api.entities.Capacitacion;
import com.liceo.api.model.AsistenciaDTO;
import com.liceo.api.model.UsuarioModel;
import com.liceo.api.repository.AsistenciaRepository;
import com.liceo.api.repository.BaseRepository;
import com.liceo.api.repository.CapacitacionRepository;
import com.liceo.api.repository.UsuariosRepository;
import com.liceo.api.service.AsistenciaService;
import com.liceo.api.service.CapacitacionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 *
 * @author USUARIO
 */
@Service
@Transactional //It is not necessary. You can use it, if you have multiple database operation in a single service method.
public class AsistenciaServiceImpl extends BaseServiceImpl<Asistencia, Integer> implements AsistenciaService {

    @Autowired
    CustomQuerys querys;
    @Autowired
    private AsistenciaRepository repository;

    public AsistenciaServiceImpl(BaseRepository<Asistencia, Integer> baseRepository) {
        super(baseRepository);
    }


    @Override
    public List<Asistencia> saveAll(AsistenciaDTO dto) throws Exception {

        List<Asistencia> a = repository.findByIdCapacitacion(dto.getCapacitacion());

        Map<Integer, UsuarioModel> map =   Arrays.stream(dto.getUsuarios()).collect(Collectors.toMap(UsuarioModel::getId_asistencia, Function.identity()));



        a.forEach(asistencia -> {
            if(map.containsKey(asistencia.getId())){
                asistencia.setEstado("A");
            }else{
                asistencia.setEstado("F");
            }
        });
        return baseRepository.saveAll(a);


    }
}
