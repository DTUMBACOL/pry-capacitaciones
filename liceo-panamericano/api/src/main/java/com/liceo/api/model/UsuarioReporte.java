package com.liceo.api.model;

import lombok.*;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class UsuarioReporte {

    String cedula;
    String nombre;
    String capacitacion;

}
