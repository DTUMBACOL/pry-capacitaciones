/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.liceo.api.repository;

import com.liceo.api.entities.Base;
import com.liceo.api.entities.Capacitacion;
import com.liceo.api.entities.Usuarios;
import java.io.Serializable;
import java.util.List;
import java.util.Optional;

import com.liceo.api.model.UsuarioModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.data.repository.query.Param;

/**
 *
 * @author USUARIO
 */
public interface UsuariosRepository extends BaseRepository<Usuarios, Integer> {

    Optional<Usuarios> findByCedula(String username);

    @Query(value = "select * from usuarios where id in ( select id_usuario from ASISTENCIA where id_capacitacion = :capacitacion )",
            nativeQuery = true)
    public List<Usuarios> findByCapacitacion( @Param("capacitacion") Integer usuario);

    @Query(value = "select u.id as id , u.nombre as nombre , a.estado as estado from usuarios u inner join ASISTENCIA a ON u.id = a.id_usuario where a.id_capacitacion =:capacitacion ",
            nativeQuery = true)
    public List<UsuarioModel> findByAsistenciaCapacitacion(@Param("capacitacion") Integer usuario);
}
