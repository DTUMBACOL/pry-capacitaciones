/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.liceo.api.model;

import lombok.Data;

/**
 *
 * @author USUARIO
 */
@Data
public class JwtResponse {

    private final String jwttoken;
    private final Integer usuario;
    private final String rol;


}
