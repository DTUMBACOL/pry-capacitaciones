/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.liceo.api.entities;

import java.util.Date;
import javax.persistence.*;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;
import org.springframework.data.annotation.CreatedDate;

/**
 *
 * @author DTUMBACO
 */
@Entity
@Table(name = "CAPACITACION")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Capacitacion  extends Base{
   
    String titulo;
    String detalle;
    String descripcion;
    String lugar;
    String fecha;
    String horario;
    int duracion;
    String modalidad;
    String tipo;
    String link;
    String estado;
    int creado_por;
    
}
