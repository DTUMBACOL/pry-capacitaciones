/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.liceo.api.controller;

import com.google.common.hash.Hashing;
import com.liceo.api.entities.Capacitacion;
import com.liceo.api.entities.Usuarios;
import com.liceo.api.jwt.JwtTokenProvider;
import com.liceo.api.service.impl.CapacitacionServiceImpl;
import com.liceo.api.service.impl.CustomQuerys;
import com.liceo.api.service.impl.UsuariosServiceImpl;
import com.liceo.api.util.SendMail;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Comparator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.mail.MessagingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 *
 * @author USUARIO
 */
@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/api/v1/capacitacion")
public class CapacitacionController extends BaseControllerImpl<Capacitacion, CapacitacionServiceImpl> {

    @Autowired
    private JwtTokenProvider jwtTokenUtil;

    private CapacitacionServiceImpl service;


    public CapacitacionController(CapacitacionServiceImpl cooperativaServiceImpl) {
        this.service = cooperativaServiceImpl;
    }

    @Override
    public ResponseEntity<?> save(@RequestBody  Capacitacion entity) {
        entity.setCreado_por(1);
        return super.save(entity);
    }

    @GetMapping("/capacitacion")
    public ResponseEntity<?> getAllCapacitacion(@RequestParam(name = "usuario") int usuario) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(servicio.findByCapacitacion(usuario));
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("");
        }
    }
    @GetMapping("/talleres")
    public ResponseEntity<?> getAllTalleres(@RequestParam(name = "usuario") int usuario) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(servicio.findByTalleres(usuario));
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("");
        }
    }

    @GetMapping("/registradas")
    public ResponseEntity<?> getRegistradas(@RequestParam(name = "usuario") int usuario ) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(servicio.findByUsuario(usuario));
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("");
        }
    }
}
