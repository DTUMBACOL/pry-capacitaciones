package com.liceo.api.controller;

import com.liceo.api.service.impl.CustomQuerys;
import com.liceo.api.util.ExportPdf;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/api/v1/reporte")
public class ReporteController {

    @Autowired
    CustomQuerys service;
    @GetMapping("")
    public ResponseEntity<?> getAllCapacitacion(@RequestParam(name = "fecha") String fecha) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(service.findReporte(fecha));
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("");
        }
    }
    @GetMapping("/export")
    public void getReport(HttpServletResponse response, @RequestParam(name = "fecha") String fecha)  throws IOException, ServletException {
        try {

            ExportPdf exportPdf = new ExportPdf(service.findReporte(fecha));

            exportPdf.export(response);

        } catch (Exception e) {
            response.setStatus(HttpStatus.BAD_REQUEST.value());
            response.flushBuffer();
        }
    }

}
