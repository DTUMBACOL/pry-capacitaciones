import { Component, Input, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { ConfirmacionComponent } from '../confirmacion/confirmacion.component';
import { ApiserviceService } from '../services/apiservice.service';

@Component({
  selector: 'app-capacitaciones',
  templateUrl: './capacitaciones.component.html',
  styleUrls: ['./capacitaciones.component.css'],
  providers: [ ApiserviceService,]
})
export class CapacitacionesComponent implements OnInit {
  @Input() deviceXs: boolean;
  topVal = 0;
  visible:boolean= false;

  capacitaciones : any[];

  onScroll(e) {
    let scrollXs = this.deviceXs ? 55 : 73;
    if (e.srcElement.scrollTop < scrollXs) {
      this.topVal = e.srcElement.scrollTop;
    } else {
      this.topVal = scrollXs;
    }
  }
  sideBarScroll() {
    let e = this.deviceXs ? 160 : 130;
    return e - this.topVal;
  }

  constructor(private service: ApiserviceService,private router: Router,
    public dialog: MatDialog, ) { }

  ngOnInit(): void {
    this.loadData();
  }



  confirmar(value){
    const dialogRef = this.dialog.open(ConfirmacionComponent,{ data: {id: value.id , nombre: value.titulo } 
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result === 1) {
        // After dialog is closed we're doing frontend updates
        // For add we're just pushing a new row inside DataService
        // this.exampleDatabase.dataChange.value.push(this.dataService.getDialogData());
        // this.refreshTable();

        this.router.navigate(["/"]);

      }
    });

  }

  loadData() {
    const token = JSON.parse(sessionStorage.getItem('token').toString());

    let data = {
      usuario: token.usuario,
    }
    this.service.get(data,'/api/v1/capacitacion/capacitacion').then(data => {
      console.log(data);
      this.capacitaciones = data;
      if(data.length>0){
        this.visible = true;
      }

    });
  }

}
