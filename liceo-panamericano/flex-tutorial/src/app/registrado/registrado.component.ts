import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { DetalleComponent } from '../detalle/detalle.component';
import { ApiserviceService } from '../services/apiservice.service';

@Component({
  selector: 'app-registrado',
  templateUrl: './registrado.component.html',
  styleUrls: ['./registrado.component.css'],
  providers: [ApiserviceService,]
})
export class RegistradoComponent implements OnInit {
  capacitaciones: any[];
  visible: boolean = false;
  admin: boolean = false;

  constructor(private service: ApiserviceService,
    public dialog: MatDialog,) {
    const token = JSON.parse(sessionStorage.getItem('token').toString());

    if (token.rol === 'ADMIN') {
      this.admin = true;
    }
    this.loadData();

  }

  ngOnInit(): void {

  }
  ver(value) {
    const dialogRef = this.dialog.open(DetalleComponent, { data: value });

    dialogRef.afterClosed().subscribe(result => {
      if (result === 1) {
        // After dialog is closed we're doing frontend updates
        // For add we're just pushing a new row inside DataService
        // this.exampleDatabase.dataChange.value.push(this.dataService.getDialogData());
        // this.refreshTable();
      }
    });

  }





  loadData() {
    const token = JSON.parse(sessionStorage.getItem('token').toString());

    var params = {
      usuario: token.usuario
    }
    if (this.admin) {
      this.service.getWithoutParams( '/api/v1/capacitacion').then(data => {
        console.log(data);

        this.capacitaciones = data;
        if (data.length > 0) {
          this.visible = true;
        }

      });
    } else {
      this.service.get(params, '/api/v1/capacitacion/registradas').then(data => {
        console.log(data);

        this.capacitaciones = data;
        if (data.length > 0) {
          this.visible = true;
        }

      });
    }

  }

}
