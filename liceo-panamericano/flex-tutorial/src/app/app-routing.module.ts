import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule, Router } from '@angular/router';
import { ContentComponent } from './content/content.component';
import { TalleresComponent } from './talleres/talleres.component';
import { IniciarSesionComponent } from './iniciar-sesion/iniciar-sesion.component';
import { CapacitacionesComponent } from './capacitaciones/capacitaciones.component';
import { RegistradoComponent } from './registrado/registrado.component';
import { CreaCapacitacionComponent } from './crea-capacitacion/crea-capacitacion.component';
import { AsistenciaComponent } from './asistencia/asistencia.component';
import { ReporteComponent } from './reporte/reporte.component';
import { AuthGuardService } from './services/auth-guard.service';
import { P404Component } from './error/404.component';
import { P500Component } from './error/500.component';
import { RegistrarseComponent } from './registrarse/registrarse.component';

const router: Routes = [

  { path: 'inicio', component: IniciarSesionComponent },
  { path: 'registro', component: RegistrarseComponent },
  {
    path: '',
    redirectTo: 'capacitaciones',
    pathMatch: 'full',
  },
  {
    path: '404',
    component: P404Component,
    data: {
      title: 'Page 404'
    }
  },
  {
    path: '500',
    component: P500Component,
    data: {
      title: 'Page 500'
    }
  },
  {
    path: '',
    component: ContentComponent,
    data: {
      title: 'Home'
    },
    children: [

      {
        path: 'capacitaciones',
        component: CapacitacionesComponent,
        data: {
          title: 'Capacitaciones'
        }
      },
      {
        path: 'talleres',
        component: TalleresComponent,
        data: {
          title: 'Talleres'
        }
      },
      {
        path: 'registrado',
        component: RegistradoComponent,
        data: {
          title: 'Registrado'
        }
      },
      {
        path: 'agregar',
        component:CreaCapacitacionComponent,
        data: {
          title: 'Crear'
        }
      },
      {
        path: 'asistencia/:id',
        component: AsistenciaComponent,
        data: {
          title: 'Asistencia'
        }
      },
      {
        path: 'reportes',
        component: ReporteComponent,
        data: {
          title: 'Reporte'
        }
      },
    ],
    canActivate: [AuthGuardService],
  },
  { path: '**', component: P404Component }
]


@NgModule({
  declarations: [],
  imports: [RouterModule.forRoot(router, { relativeLinkResolution: 'legacy' })],

  exports: [RouterModule]
})
export class AppRoutingModule { }
