import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ApiserviceService } from '../services/apiservice.service';

@Component({
  selector: 'app-iniciar-sesion',
  templateUrl: './iniciar-sesion.component.html',
  styleUrls: ['./iniciar-sesion.component.css'],
  
  providers: [ApiserviceService]
})
export class IniciarSesionComponent implements OnInit {
  usuario = "";
  clave = "";
  constructor(private service: ApiserviceService,private router: Router) { }


  ngOnInit(): void {
    sessionStorage.clear();

  }

  login() {

    let data = {
      username: this.usuario,
      password: this.clave
    }
    this.service.login(data).subscribe(
      res => {
        console.log(res);
        let token = res.jwttoken
        if (token) {
          sessionStorage.setItem("token", JSON.stringify(res))
          sessionStorage.setItem("usuario", res.usuario)
          sessionStorage.setItem("rol", res.rol)
          this.router.navigate(["/"]);

        }


      },
      error => {
        console.log(error);
      }

    );
  }

}
