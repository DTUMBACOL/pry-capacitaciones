import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgbCalendar, NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';
import * as moment from 'moment';
import { ApiserviceService } from '../services/apiservice.service';

@Component({
  selector: 'app-reporte',
  templateUrl: './reporte.component.html',
  styleUrls: ['./reporte.component.css']
})
export class ReporteComponent implements OnInit {
  model: NgbDateStruct;
  date: { year: number, month: number };

  selectedDate: Date = new Date();
  capacitaciones: any[];
  docentes: any[];
  horas: any = {
    "horaCapacitacion": 0,
    "horaTalleres": 0
  }
  usuarios: any[];
  talleres: any[];
  maxDate = new Date();

  constructor(private service: ApiserviceService, private router: Router, private calendar: NgbCalendar) {
  }
  ngOnInit(): void {

    this.loadData(this.selectedDate);

  }



  selectToday() {
    this.model = this.calendar.getToday();
  }

  dateChanged(event) {
    console.log("Date changed", event);
    this.loadData(event);
    //handler logic
  }

  formatDate(loading_date) {

    return moment(loading_date).format("YYYY-MM-DD");
  }

  descargar() {
    let params = {
      fecha: this.formatDate(this.selectedDate),
    }
    let tab = window.open();

    this.service.downloadPDF(params).subscribe(data => {

      const fileUrl = URL.createObjectURL(data);
      tab.location.href = fileUrl;

    });
  }
  loadData(fecha) {

    let data = {
      fecha: this.formatDate(fecha),
    }
    this.service.get(data, '/api/v1/reporte').then(data => {
      console.log(data);
      this.docentes = data.docentes;
      this.talleres = data.talleres;
      this.capacitaciones = data.capacitaciones;
      this.horas = data.horas;
    });
  }
}
