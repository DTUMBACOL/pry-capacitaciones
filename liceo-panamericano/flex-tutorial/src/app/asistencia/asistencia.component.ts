import { AfterViewChecked, ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, ValidatorFn } from '@angular/forms';
import { MatListOption } from '@angular/material/list';
import { ActivatedRoute, Router } from '@angular/router';
import { element } from 'protractor';
import { Capacitacion } from '../models/capacitacion';
import { ApiserviceService } from '../services/apiservice.service';

@Component({
  selector: 'app-asistencia',
  templateUrl: './asistencia.component.html',
  styleUrls: ['./asistencia.component.css']
})
export class AsistenciaComponent implements OnInit, AfterViewChecked {

  usuarios: any[];
  loading: boolean = true;
  selectedAsistencia: any[];

  capacitacion;
  constructor(private _Activatedroute: ActivatedRoute, private service: ApiserviceService,
    private _router: Router, private changeRef: ChangeDetectorRef) {

  }
  ngOnInit(): void {

    this._Activatedroute.paramMap.subscribe(params => {
      console.log(params);
      let capacitacion = params.get('id');
      this.loadData(capacitacion);
    });
  }

  ngAfterViewChecked(): void { this.changeRef.detectChanges(); }

  loadData(capacitacion) {

    let data = {
      capacitacion: capacitacion
    }
    this.capacitacion = capacitacion;
    this.service.get(data, '/api/v1/usuarios/capacitacion').then(data => {
      console.log(data);
      this.usuarios = data;
      this.loading = false;

    });
  }

  confirmar() {

    this.selectedAsistencia.forEach(element => {
      console.log(element);

    });

    let data = {
      usuarios: this.selectedAsistencia,

      capacitacion: this.capacitacion
    }
    this.service.subscribeAsistencia(data).subscribe(
      res => {
        console.log(res);
      },
      error => {
        console.log(error);
      }

    )

  }
}
// function
