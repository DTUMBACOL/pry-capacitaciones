import { Component, Input } from "@angular/core";
import { MatDialog } from "@angular/material/dialog";
import { Router } from "@angular/router";
import { CreaCapacitacionComponent } from "../crea-capacitacion/crea-capacitacion.component";

@Component({
  selector: "app-header",
  templateUrl: "./header.component.html",
  styleUrls: ["./header.component.css"]
})
export class HeaderComponent {
  @Input() deviceXs: boolean;

  admin: boolean = false;
  constructor( private router: Router) {

    const token = JSON.parse(sessionStorage.getItem('token').toString());
    if( token.rol === 'ADMIN'){
      this.admin = true;
    }
   }

    addNew() {
      this.router.navigate(["agregar"]);

    }
  

}

