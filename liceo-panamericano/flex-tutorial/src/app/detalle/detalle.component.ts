import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { Capacitacion } from '../models/capacitacion';
import { ApiserviceService } from '../services/apiservice.service';

@Component({
  selector: 'app-detalle',
  templateUrl: './detalle.component.html',
  styleUrls: ['./detalle.component.css'],
  providers: [ ApiserviceService,]
})
export class DetalleComponent  {
  admin : boolean = false;
  usuarios : any[];

  constructor(public dialogRef: MatDialogRef<DetalleComponent>,
    @Inject(MAT_DIALOG_DATA) public data: Capacitacion, private service: ApiserviceService,private router: Router)
  {
    const token = JSON.parse(sessionStorage.getItem('token').toString());
    if( token.rol === 'ADMIN'){
      this.admin = true;
    }
  }


  ngOnInit(): void {

    this.loadData();

  }
  ver(){
    this.router.navigate(["asistencia"]);

  }

  loadData() {

    let data = {
      capacitacion: this.data.id
    }
    this.service.get(data,'/api/v1/usuarios/capacitacion').then(data => {
      console.log(data);
      this.usuarios = data;
    });
  }

}
