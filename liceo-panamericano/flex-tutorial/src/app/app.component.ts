import { Component, OnInit, OnDestroy } from '@angular/core';
import { MediaObserver, MediaChange } from '@angular/flex-layout';
import { Router, NavigationEnd } from '@angular/router';
import { Subscription } from 'rxjs';
import { BnNgIdleService } from 'bn-ng-idle';
import { AuthService } from './services/auth.service';

@Component({
  selector: 'app-root',
  template: '<router-outlet></router-outlet>',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, OnDestroy {
  title = 'flex-tutorial';
  mediaSub: Subscription;
  deviceXs: boolean;
  constructor(public mediaObserver: MediaObserver,
    private router: Router,
    private bnIdle: BnNgIdleService, 
    public auth: AuthService) {
      this.bnIdle.startWatching(300).subscribe((res) => {
        if (res) {
          if (this.auth.isAuthenticated()) {
            this.router.navigate(['inicio']);
  
            console.log("session expired " + new Date());
  
          }
  
        }
      })
  }
  ngOnInit() {
    this.mediaSub = this.mediaObserver.media$.subscribe((res: MediaChange) => {
      console.log(res.mqAlias);
      this.deviceXs = res.mqAlias === "xs" ? true : false;
    });
    this.router.events.subscribe((evt) => {
      if (!(evt instanceof NavigationEnd)) {
        return;
      }
      window.scrollTo(0, 0);
    });
  }
  ngOnDestroy() {
    this.mediaSub.unsubscribe();
  }
}
