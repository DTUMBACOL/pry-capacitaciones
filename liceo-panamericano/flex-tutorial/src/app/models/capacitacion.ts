export class Capacitacion {
  id: number;
  titulo: string;
  detalle: string;
  descripcion: string;
  lugar: string;
  fecha: string;
  horario: string;
  duracion: number;
  modalidad: string;
  tipo: string;
  link: string;
  estado: string;
  creado_por: number;
}