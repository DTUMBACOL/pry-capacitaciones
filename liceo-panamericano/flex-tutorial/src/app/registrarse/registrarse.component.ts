import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ApiserviceService } from '../services/apiservice.service';

@Component({
  selector: 'app-registrarse',
  templateUrl: './registrarse.component.html',
styleUrls: ['./registrarse.component.css']
})
export class RegistrarseComponent implements OnInit {

  usuario = "";
  email = "";
  constructor(private service: ApiserviceService,private router: Router) { }

  ngOnInit(): void {
  }

  login() {

    let data = {
      cedula: this.usuario,
      email: this.email
    }
    this.service.registro(data).subscribe(
      res => {
        console.log(res);

        this.router.navigate(["/inicio"]);


      },
      error => {
        console.log(error);
      }

    );
  }
}
