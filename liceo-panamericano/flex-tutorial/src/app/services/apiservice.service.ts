import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ApiserviceService {

  constructor(private http: HttpClient) { }

  login(data) {
    let myHeaders: HttpHeaders = new HttpHeaders({
      "Content-Type": "application/json",
    });
    let body = JSON.stringify(data);
    return this.http.post<any>(
      `${environment.service}/api/user/authenticate`,
      body,
      { headers: myHeaders }
    );
  }
  subscribeActividad(data) {
    let myHeaders: HttpHeaders = new HttpHeaders({
      "Content-Type": "application/json",
    });
    let body = JSON.stringify(data);
    return this.http.post<any>(
      `${environment.service}/api/v1/asistencia`,
      body,
      { headers: myHeaders }
    );
  }
  subscribeAsistencia(data) {
    let myHeaders: HttpHeaders = new HttpHeaders({
      "Content-Type": "application/json",
    });
    let body = JSON.stringify(data);
    return this.http.post<any>(
      `${environment.service}/api/v1/asistencia/registra`,
      body,
      { headers: myHeaders }
    );
  }

  registro(data) {
    let myHeaders: HttpHeaders = new HttpHeaders({
      "Content-Type": "application/json",
    });
    let body = JSON.stringify(data);
    return this.http.post<any>(
      `${environment.service}/api/v1/usuarios`,
      body,
      { headers: myHeaders }
    );
  }

  creaActividad(data) {
    let myHeaders: HttpHeaders = new HttpHeaders({
      "Content-Type": "application/json",
    });
    let body = JSON.stringify(data);
    return this.http.post<any>(
      `${environment.service}/api/v1/capacitacion`,
      body,
      { headers: myHeaders }
    );
  }

  get(params, url) {

    const token = JSON.parse(sessionStorage.getItem('token').toString());
    let myHeaders: HttpHeaders = new HttpHeaders({
      "Content-Type": "application/json",
      "Authorization": "Bearer " + token.jwttoken,

    });

    return this.http.get<any>(
      `${environment.service}` + url,
      {
        headers: myHeaders
        , params: params
      }).toPromise();
  }


  downloadPDF(params): any {
    const token = JSON.parse(sessionStorage.getItem('token').toString());

    let myHeaders: HttpHeaders = new HttpHeaders({
      "Content-Type": "application/json",
      "Authorization": "Bearer " + token.jwttoken,

    });
    return this.http.get(`${environment.service}/api/v1/reporte/export`, {
      headers: myHeaders
      , params: params,
      responseType: 'blob'
    }).pipe(map(
      (res) => {
        return new Blob([res], { type: 'application/pdf' })
      }));

  }

  getWithoutParams(url) {

    const token = JSON.parse(sessionStorage.getItem('token').toString());
    let myHeaders: HttpHeaders = new HttpHeaders({
      "Content-Type": "application/json",
      "Authorization": "Bearer " + token.jwttoken,

    });

    return this.http.get<any>(
      `${environment.service}` + url,
      {
        headers: myHeaders
      }).toPromise();
  }
}
