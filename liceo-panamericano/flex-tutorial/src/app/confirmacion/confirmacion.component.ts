import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { Actividad } from '../models/actividad';
import { ApiserviceService } from '../services/apiservice.service';

@Component({
  selector: 'app-confirmacion',
  templateUrl: './confirmacion.component.html',
  styleUrls: ['./confirmacion.component.css'],

  providers: [ApiserviceService]
})
export class ConfirmacionComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<ConfirmacionComponent>,
    @Inject(MAT_DIALOG_DATA) public data: Actividad, private service: ApiserviceService, private router: Router) { }

  ngOnInit(): void {
  }

  registrar() {
    const token = JSON.parse(sessionStorage.getItem('token').toString());

    let data = {
      id_usuario: token.usuario,
      id_capacitacion: this.data.id,
      estado: "R"
    }
    this.service.subscribeActividad(data).subscribe(res => {
      console.log(res);

    },
      error => {
        console.log(error);
      });
  }
}
