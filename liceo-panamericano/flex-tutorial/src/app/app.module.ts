import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatIconModule } from '@angular/material/icon';
import { MatMenuModule } from '@angular/material/menu';
import { MatButtonModule } from '@angular/material/button';
import { MatNativeDateModule, MatRippleModule } from '@angular/material/core';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatCardModule } from '@angular/material/card';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatListModule } from '@angular/material/list';
import { MatDialogModule } from '@angular/material/dialog';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatRadioModule} from '@angular/material/radio';
import {ScrollingModule} from '@angular/cdk/scrolling';
import {MatSelectModule} from '@angular/material/select';
import { FormsModule } from '@angular/forms'; 
import { ReactiveFormsModule } from '@angular/forms';
import {MatTabsModule} from '@angular/material/tabs';

import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { AppRoutingModule } from './app-routing.module';
import { ContentComponent } from './content/content.component';
import { TalleresComponent } from './talleres/talleres.component';
import { DetalleComponent } from './detalle/detalle.component';
import { NuevoComponent } from './nuevo/nuevo.component';
import { RegistrarseComponent } from './registrarse/registrarse.component';
import { IniciarSesionComponent } from './iniciar-sesion/iniciar-sesion.component';
import { RegistradoComponent } from './registrado/registrado.component';
import { AsistenciaComponent } from './asistencia/asistencia.component';
import { CapacitacionesComponent } from './capacitaciones/capacitaciones.component';
import { LocationStrategy, PathLocationStrategy } from '@angular/common';
import { CreaCapacitacionComponent } from './crea-capacitacion/crea-capacitacion.component';
import { NgxMaterialTimepickerModule } from 'ngx-material-timepicker';
import { ConfirmacionComponent } from './confirmacion/confirmacion.component';
import { ReporteComponent } from './reporte/reporte.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AuthService } from './services/auth.service';
import { AuthGuardService } from './services/auth-guard.service';
import { BnNgIdleService } from 'bn-ng-idle';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    ContentComponent,
    TalleresComponent,
    DetalleComponent,
    NuevoComponent,
    RegistrarseComponent,
    IniciarSesionComponent,
    RegistradoComponent,
    AsistenciaComponent,
    CapacitacionesComponent,
    CreaCapacitacionComponent,
    ConfirmacionComponent,
    ReporteComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    BrowserAnimationsModule,
    FlexLayoutModule,
    MatToolbarModule,
    MatIconModule,
    FormsModule,
    MatMenuModule,
    MatSelectModule,
    ReactiveFormsModule,
    MatButtonModule,
    MatDatepickerModule,
    MatNativeDateModule,
    AppRoutingModule,
    MatRippleModule,
    MatFormFieldModule,
    MatInputModule,
    MatDialogModule,
    NgxMaterialTimepickerModule,
    MatSidenavModule,
    ScrollingModule,
    MatTabsModule,
    MatCardModule,
    MatListModule,
    MatRadioModule,
    NgbModule
  ],
  providers: [
    {
      provide: LocationStrategy,
      useClass: PathLocationStrategy
    }, AuthGuardService, AuthService, BnNgIdleService],
  bootstrap: [AppComponent]
})
export class AppModule { }
