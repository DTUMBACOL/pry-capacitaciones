import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import * as moment from 'moment';
import { ApiserviceService } from '../services/apiservice.service';

@Component({
  selector: 'app-crea-capacitacion',
  templateUrl: './crea-capacitacion.component.html',
  styleUrls: ['./crea-capacitacion.component.css'],
  providers: [ApiserviceService]
})
export class CreaCapacitacionComponent implements OnInit {

  selected: string;
  fecha;
  user: FormGroup;

  item: any = {
    "titulo": "",
    "detalle": "",
    "descripcion": "",
    "lugar": "",
    "fecha": "",
    "horario": "",
    "duracion": 0,
    "modalidad": "",
    "tipo": "",
    "link": "",
    "estado": "A"
  }

  ngOnInit(): void {

    this.user = new FormGroup({
      tituloValidator: new FormControl('', [Validators.required]),
      detalleValidator: new FormControl('', [Validators.required]),
      descripcionValidator: new FormControl('', [Validators.required]),
      fechaValidator: new FormControl('', [Validators.required]),
      horaValidator: new FormControl('', [Validators.required]),
      lugarValidator: new FormControl('', [Validators.required]),
      duracionValidator: new FormControl('', [Validators.required]),
      tipoValidator: new FormControl('', [Validators.required])

    });
  }
  constructor(private service: ApiserviceService, private router: Router) { }

  registrar() {

    if (!this.user.valid) {
      alert('Llene todos los datos ')

    } else {
      switch (this.selected) {

        case "1":
          this.item.modalidad = "P"
          this.item.tipo = "C"
          break;
        case "2":
          this.item.modalidad = "O"
          this.item.tipo = "C"

          break;
        case "3":
          this.item.modalidad = "P"
          this.item.tipo = "T"

          break;
        case "4":
          this.item.modalidad = "O"
          this.item.tipo = "T"

          break;

      }
      this.item.fecha = this.formatDate(this.item.fecha);
      console.log(this.item);

      this.service.creaActividad(this.item).subscribe(
        res => {
          console.log(res);
          let id = res.id
          if (id) {
            if (this.item.tipo === 'T')
              this.router.navigate(["/talleres"]);
            else
              this.router.navigate(["/capacitaciones"]);

          } else {

            alert('No se pudo almacenar correctamente, intente mas tarde')
          }


        },
        error => {
          console.log(error);
        }

      );
    }
  }
  formatDate(loading_date) {

    return moment(loading_date).format("YYYY-MM-DD");
  }



}
