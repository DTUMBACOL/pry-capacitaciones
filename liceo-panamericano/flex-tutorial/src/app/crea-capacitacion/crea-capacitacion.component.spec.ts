import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreaCapacitacionComponent } from './crea-capacitacion.component';

describe('CreaCapacitacionComponent', () => {
  let component: CreaCapacitacionComponent;
  let fixture: ComponentFixture<CreaCapacitacionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreaCapacitacionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreaCapacitacionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
